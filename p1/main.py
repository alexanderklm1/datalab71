import psycopg2
import time


time.sleep(20)

conn = psycopg2.connect(
    dbname="root",
    user="root",
    password="root",
    host="172.19.0.2",
    port="5432"
)

cur = conn.cursor()

query = """
    SELECT * FROM students;
 """ 


cur.execute(query)
rows = cur.fetchall()

with open('artifact1.txt', 'w') as file:
    for row in rows:
        file.write(f"{row[0]}\t{row[1]}\t{row[2]}\t{row[3]}\t{row[4]}\n")

cur.close()
conn.close()

