import os

def count_digits(n):
  s = str(n).split(".")
  if len(s) == 2:
    return len(s[1])
  else:
    return 0

def main():
    x = float(os.environ.get("Num1"))
    y = float(os.environ.get("Num2"))
    if count_digits(x) > count_digits(y):
        t = f"В числе {x} больше цифр после запятой, чем в числе {y}"
    elif count_digits(x) < count_digits(y):
        t = f"В числе {y} больше цифр после запятой, чем в числе {x}"
    else:
        t = f"В числах {x} и {y} одинаковое количество цифр после запятой"
    print(t)
    with open("artifact7.txt", "w") as c:
      c.write(t)
      c.close()

main()
