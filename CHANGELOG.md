# CHANGELOG



## v1.0.0 (2024-01-07)


## v0.6.2 (2024-01-07)


## v0.6.1 (2024-01-07)


## v0.6.0 (2024-01-07)


## v0.5.0 (2024-01-07)


## v0.4.0 (2024-01-07)


## v0.3.0 (2024-01-07)


## v0.2.0 (2024-01-07)


## v0.1.0 (2024-01-07)


## v0.0.0 (2024-01-07)

### Unknown

* Merge branch &#39;dev&#39; into &#39;main&#39;

Dev2

See merge request alexanderklm1/datalab71!2 ([`1212118`](https://github.com/alexanderklm1/datalab71/commit/1212118fa191292c8a6b6947b9a5b68258d425b3))

* Update setup.py ([`b78ca12`](https://github.com/alexanderklm1/datalab71/commit/b78ca12396a0982f2b9b79bca33f3b51aabf0464))

* Update pyproject.toml ([`fe77d55`](https://github.com/alexanderklm1/datalab71/commit/fe77d55443d5e794cd1fde98364d5c5f84787f47))

* Update pytproject.toml ([`3e04f82`](https://github.com/alexanderklm1/datalab71/commit/3e04f82a19d0b74c7d060f03381eea0f40b52e74))

* Add new file ([`65fbf76`](https://github.com/alexanderklm1/datalab71/commit/65fbf769c8937033c99f0fb09de1677b57d7cb74))

* Add new file ([`be766ff`](https://github.com/alexanderklm1/datalab71/commit/be766ff01844474c8b236370a016d5424e6fb809))

* Update README.md ([`c0252ef`](https://github.com/alexanderklm1/datalab71/commit/c0252efb6209b269533f379dce5d7a47b07af1f4))

* Merge branch &#39;dev&#39; into &#39;main&#39;

Dev

See merge request alexanderklm1/datalab71!1 ([`abfef0e`](https://github.com/alexanderklm1/datalab71/commit/abfef0e9c82787f2eb5bf45bf8382f35f6aff1dd))

* Update README.md ([`0d64405`](https://github.com/alexanderklm1/datalab71/commit/0d644054dd5107de6fe002ebc0052545aa978921))

* Update main.py ([`c299b55`](https://github.com/alexanderklm1/datalab71/commit/c299b5563d2a6180647dc7a440539ae9d1ccfb36))

* Update file main.py ([`f3fe5da`](https://github.com/alexanderklm1/datalab71/commit/f3fe5daf943d354e1a046bb598fb95bdf0eb9a4e))

* Update file main.py ([`38138ee`](https://github.com/alexanderklm1/datalab71/commit/38138ee43325b92eebfee851f3162e2a6117704a))

* Update main.py ([`314121c`](https://github.com/alexanderklm1/datalab71/commit/314121ca7d51c6fd4807f85b9046f620c962d255))

* Update main.py ([`5d20683`](https://github.com/alexanderklm1/datalab71/commit/5d20683c65b9c143e5298b0801a28e8c8cb48048))

* Update main.py ([`8ba4d57`](https://github.com/alexanderklm1/datalab71/commit/8ba4d57d5173a870cd669a5613d9313baff86ea9))

* Update init.sql ([`7a427b5`](https://github.com/alexanderklm1/datalab71/commit/7a427b56667a2bae6b4934630de3983bfc4f2618))

* Update main.py ([`1bab54c`](https://github.com/alexanderklm1/datalab71/commit/1bab54c41c7ce4acdd99c0120c81016efc33aa68))

* Update .gitlab-ci.yml ([`5bc9720`](https://github.com/alexanderklm1/datalab71/commit/5bc9720bb7eab2791ed2dc6f5ce1ca0d677f33af))

* Update init.sql ([`a1c0a78`](https://github.com/alexanderklm1/datalab71/commit/a1c0a787b7ff1b5445489b3322716484ded5f296))

* Update Dockerfile ([`72c0534`](https://github.com/alexanderklm1/datalab71/commit/72c0534baf9b2bac8b9a9ee87b7f407d2ae85d13))

* Update .gitlab-ci.yml ([`3fae999`](https://github.com/alexanderklm1/datalab71/commit/3fae999fd44c92aaa7d1d3264183a678a5c00489))

* Update main.py ([`038d764`](https://github.com/alexanderklm1/datalab71/commit/038d76464a83f9c7776fa9056d7177d1597d6519))

* Update .gitlab-ci.yml ([`1cbf277`](https://github.com/alexanderklm1/datalab71/commit/1cbf27794f4e14c8c4f6a2fd405d330aad534b61))

* Update docker-compose.yml ([`aac2986`](https://github.com/alexanderklm1/datalab71/commit/aac2986068a2ba8c029beee9ed6533bb86395055))

* Update docker-compose.yml ([`0a378cc`](https://github.com/alexanderklm1/datalab71/commit/0a378cc98a280a2215c77513eacdaadfe8953998))

* Update .gitlab-ci.yml ([`ad93b49`](https://github.com/alexanderklm1/datalab71/commit/ad93b497a50cf17fd85205a0e6a7ee90c8193ccb))

* Update .gitlab-ci.yml ([`07ffbc1`](https://github.com/alexanderklm1/datalab71/commit/07ffbc1c0ce05cc2bb360548cb0210a91e12742e))

* Update file main.py ([`3c42a9c`](https://github.com/alexanderklm1/datalab71/commit/3c42a9ced16cb4538543f78467d88a146d731576))

* Delete init.sql ([`2f71794`](https://github.com/alexanderklm1/datalab71/commit/2f71794a7a0fddc1e96d236afac0dd6d7a2d1e11))

* Add new file ([`dbacc56`](https://github.com/alexanderklm1/datalab71/commit/dbacc569f342a7062fbaa5939c6543ff55ac5c23))

* Add new file ([`3949275`](https://github.com/alexanderklm1/datalab71/commit/3949275945df0ea9bf43a7e25674b7cbd2ffb674))

* Add new file ([`f98a196`](https://github.com/alexanderklm1/datalab71/commit/f98a19605feef7d4a01b41d8ea925e35aa393fe1))

* Update .gitlab-ci.yml ([`c871277`](https://github.com/alexanderklm1/datalab71/commit/c8712774ab76d6674b10ba7e6bd731f85340f2f4))

* Update .gitlab-ci.yml ([`2308970`](https://github.com/alexanderklm1/datalab71/commit/23089700912fd4c4b20b8191105ef3101a542e8b))

* Update .gitlab-ci.yml ([`7a29a8f`](https://github.com/alexanderklm1/datalab71/commit/7a29a8fb553afc92427e788fdbaa6d6894153568))

* Add new file ([`bf09a45`](https://github.com/alexanderklm1/datalab71/commit/bf09a455bb41996d8605cc174f125143b9ce29b4))

* Update .gitlab-ci.yml ([`5eec5a4`](https://github.com/alexanderklm1/datalab71/commit/5eec5a473e8cb27a5f9cd5dbc479f4d508ad8882))

* Add new file ([`aa9046a`](https://github.com/alexanderklm1/datalab71/commit/aa9046a758541fccced779ec783d12aed7481968))

* Add new directory ([`b6f5108`](https://github.com/alexanderklm1/datalab71/commit/b6f51087dcdbb887544d1a9d6af0ea74e5349802))

* Update README.md ([`f3e29cf`](https://github.com/alexanderklm1/datalab71/commit/f3e29cfaa528503b8c6a124493bb76390f41fab7))

* Update README.md ([`21fba45`](https://github.com/alexanderklm1/datalab71/commit/21fba451ca38715e5babd54a2cac6a23f68db825))

* Add new file ([`17b3a3c`](https://github.com/alexanderklm1/datalab71/commit/17b3a3cacaa3a941991dd3ced8b8cf5b2f8cdf1a))

* Add new file ([`760f536`](https://github.com/alexanderklm1/datalab71/commit/760f5365eb4dc35606e90d035ebe9b8d924cbb52))

* Add new directory ([`df1ef28`](https://github.com/alexanderklm1/datalab71/commit/df1ef28a9341c86176c4eb68f208adb578a34599))

* Add new file ([`d9ca975`](https://github.com/alexanderklm1/datalab71/commit/d9ca97539e4c71132458fbec30bafb268a93e1d6))

* Initial commit ([`419cc5c`](https://github.com/alexanderklm1/datalab71/commit/419cc5c96a16a6883b61e2ecf31e7b09e5f8ef0b))
